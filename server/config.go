package server

import (
	"sync"
	"github.com/joho/godotenv"
	"os"
	"fmt"
	"strconv"
)

type ConfigManager struct {
	Properties map[string]interface{}
}

var configManager *ConfigManager
var once sync.Once

func GetConfigManager() *ConfigManager {
	once.Do(func() {
		configManager = &ConfigManager{Properties:make(map[string]interface{})}
	})

	return configManager
}

func CheckEnvVar(key string) error {
	_, result := os.LookupEnv(key)

	if !result {
		return fmt.Errorf("cannot find environment variable: %s", key)
	}

	return nil
}

func (cm *ConfigManager) Load() error {
	err := godotenv.Load()

	if err != nil { return err }

	for _, key := range []string{
		"DB_NAME",
		"DB_USER",
		"DB_PASSWORD",
		"DB_TYPE",
		"ENABLE_DEBUG",
	} {
		checkErr := CheckEnvVar(key)

		if checkErr != nil {
			return checkErr
		}

		cm.Properties[key] = os.Getenv(key)
	}

	return nil
}

func (cm *ConfigManager) GetIntProperty(key string) (int, error) {
	v, e := cm.Properties[key]

	if !e {
		return 0, fmt.Errorf("unknown property: %s", key)
	}

	pi, _ := strconv.ParseInt(v.(string), 10,32)

	return int(pi), nil
}

func (cm *ConfigManager) GetStringProperty(key string) (string, error) {
	v, e := cm.Properties[key]

	if !e {
		return "", fmt.Errorf("unknown property: %s", key)
	}

	return v.(string), nil
}

func (cm *ConfigManager) FetchStringProperty(key string) string {
	v, e := cm.GetStringProperty(key)

	if e != nil { panic(e) }

	return v
}

func (cm *ConfigManager) GetFloatProperty(key string) (float32, error) {
	v, e := cm.Properties[key]

	if !e {
		return 0.0, fmt.Errorf("unknown property: %s", key)
	}

	pf, _ := strconv.ParseFloat(v.(string), 32)

	return float32(pf), nil
}

func (cm *ConfigManager) FetchFloatProperty(key string) float32 {
	v, e := cm.GetFloatProperty(key)

	if e != nil { panic(e) }

	return v
}

func (cm *ConfigManager) GetBoolProperty(key string) (bool, error) {
	v, e := cm.Properties[key]

	if !e {
		return false, fmt.Errorf("unknown property: %s", key)
	}

	pb, _ := strconv.ParseBool(v.(string))
	return pb, nil
}

func (cm *ConfigManager) FetchBoolProperty(key string) bool {
	v, e := cm.GetBoolProperty(key)

	if e != nil { panic(e) }

	return v
}