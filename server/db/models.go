package db

import (
	"github.com/jinzhu/gorm"
)

type ServerConfigModel struct {
	gorm.Model
}

type UserModel struct {
	gorm.Model

	Email     string
	Password  string
	IPAddress string

	Personas []PersonaModel `gorm:"foreignkey:UserID"`
}

type PersonaModel struct {
	gorm.Model

	Name     string
	Motto    string
	Icon     uint
	Level    uint
	Rep      uint
	LevelRep uint
	UserID   uint
	Score    uint
	User     UserModel `gorm:"foreignkey:UserID"`
}
