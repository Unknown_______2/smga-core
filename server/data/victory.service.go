package data

import "time"

// This file defines the schemas for all data structures in the Victory.Service namespace.

type SystemInfo struct {
	Branch                 string    `xml:"Branch"`
	ChangeList             string    `xml:"ChangeList"`
	ClientVersion          string    `xml:"ClientVersion"`
	ClientVersionCheck     bool      `xml:"ClientVersionCheck"`
	Deployed               string    `xml:"Deployed"`
	EntitlementsToDownload bool      `xml:"EntitlementsToDownload"`
	ForcePermanentSession  bool      `xml:"ForcePermanentSession"`
	JidPrepender           string    `xml:"JidPrepender"`
	LauncherServiceUrl     string    `xml:"LauncherServiceUrl"`
	NucleusNamespace       string    `xml:"NucleusNamespace"`
	NucleusNamespaceWeb    string    `xml:"NucleusNamespaceWeb"`
	PersonaCacheTimeout    int       `xml:"PersonaCacheTimeout"`
	PortalDomain           string    `xml:"PortalDomain"`
	PortalSecureDomain     string    `xml:"PortalSecureDomain"`
	PortalStoreFailurePage string    `xml:"PortalStoreFailurePage"`
	PortalTimeOut          int       `xml:"PortalTimeOut"`
	ShardName              string    `xml:"ShardName"`
	Time                   time.Time `xml:"Time"`
	Version                string    `xml:"Version"`
}
